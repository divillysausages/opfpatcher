# README #

This is a simple command line tool for patching opf files present in EPUBs so that they get all the extra features when converting to MOBI.

You point it towards an EPUB and it will unzip it, patch in the relevant details, and rezip it under a new name.

## Features ##

* Optionally remove the cover declaration. As of right now, Amazon recommends that you upload the cover separately and it'll combine the two when processing your MOBI file. When keeping the cover in, I had problems getting the *Cover* link to work in Kindle for Android, though that might be another bug. Making this optional however, lets you produce MOBI files for distribution outside of Amazon, with the cover intact
* Optionally add a link for the start of the book so that when readers first open, it starts in the right area
* Optionally add a link for the dedication section
* Optionally add a link for the acknowledgements section
* Optionally add a link for the back-of-the-book section

## How do I get set up? ##

All interaction is done via the command line, making it a perfect fit for a step in your build chain. 

Once downloaded, there are two ways to use the OPFPatcher:

### Use a pre-built exe ###

On the [Downloads](https://bitbucket.org/divillysausages/opfpatcher/downloads/) page, you'll find exes for your target platform.

Simply target the exe in question in your batch file/command prompt and you're good to go (see **Usage** below)

### Work from source ###

1. You'll need [Node](https://nodejs.org/en/) installed
2. Download the project and unzip it to the folder of your choice
3. Open a command line inside the folder (`cmd` or `bash`)
4. Type `npm install` or `npm install -g` to install all the dependencies. Using `npm install -g` will allow you to call the OPFPatcher from anywhere using the `opfpatch` command
5. If installed locally, usage is in the form `node ./src/index.js [arguments]`
6. If installed globally, usage is in the form `opfpatch [arguments]`

## Usage ##

Use `opfpatch --help` to get a list of all the commands and what they do. 

A full example would be:

`opfpatch -i PATH/TO/EPUB_FILE.epub --book-title "Chapter 1" --book-title-opf "Book" --ded-title "Dedication" --ded-title-opf "Dedications" --ack-title "Thanks to" --ack-title-opf "Acknowledgements" --bob-title "About me" --bob-title-opf "About the author" --keep-cover`

This will result in a new EPUB with the name and location `PATH/TO/EPUB_FILE_patched.epub`.

For each of the extra sections (`--book-title`, `--ded-title`, `--ack-title`, and `--bob-title`), you supply the chapter heading for the chapter that you wish to use. The tool will parse through your EPUB to find the correct link, even if it's all one file. 

The correspoding `[...]-title-opf` options refer to what each section will be titled in the guide. 

So `--book-title "Chapter 1" --book-title-opf "Start here"` would look for the file inside your EPUB with the chapter heading `"Chapter 1"` and create a guide link for it with the title `"Start here"`.

If you’re using a pre-built exe, then replace the `opfpatch` command with `OPFPatcher-win-x64.exe` or `OPFPatcher-mac.app` respectively.

If you've installed the project locally, then replace the `opfpatch` with `node ./src/index.js`.