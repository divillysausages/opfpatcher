#!/usr/bin/env node

const fs = require( 'fs' );
const path = require( 'path' );
const AdmZip = require( 'adm-zip' );
const fileReader = require( './fileReader' );
const simpleXML = require( './simpleXML' );

// the different command line arguments that we're defining
const args = [
	{
		name: 'i',
		num: 1,
		desc: "The EPUB to load",
		req: true, // required
	},
	{
		name: 'book-title',
		num: 1,
		desc: "The title of the file/section to use for the start of the book (e.g. 'Chapter 1', '1')"
	},
	{
		name: 'book-title-opf',
		num: 1,
		desc: "The title to use for the book section in the OPF",
		default: 'Book'
	},
	{
		name: 'ded-title',
		num: 1,
		desc: "The title of the file/section to use for the start of the book (e.g. 'Dedication')"
	},
	{
		name: 'ded-title-opf',
		num: 1,
		desc: "The title to use for the dedication section in the OPF",
		default: 'Dedication'
	},
	{
		name: 'ack-title',
		num: 1,
		desc: "The title of the file/section to use for the start of the book (e.g. 'Acknowledgments', 'Thanks')"
	},
	{
		name: 'ack-title-opf',
		num: 1,
		desc: "The title to use for the acknowledgements section in the OPF",
		default: "Acknowledgements"
	},
	{
		name: 'bob-title',
		num: 1,
		desc: "The title of the file/section to use for the back-of-book (e.g. 'About the author')"
	},
	{
		name: 'bob-title-opf',
		num: 1,
		desc: "The title to use for the back-of-book section in the OPF",
		default: "About the author"
	},
	{
		name: 'keep-cover',
		num: 0,
		desc: "If set, then the cover won't be removed (good for distributing outside of Amazon)",
		default: false
	}
]

// capture our command-line arguments
const yargs = require( 'yargs' );

// set our arguments
args.forEach( arg => {
	if ( arg.req ) {
		yargs.demand( arg.name );
	}
	if ( arg.num ) {
		yargs.nargs( arg.name, arg.num );
	} else {
		yargs.boolean( arg.name )
	}
	yargs.describe( arg.name, arg.desc );
	if ( 'default' in arg ) {
		yargs.default( arg.name, arg.default );
	}
} )

const argv = yargs
	.usage( 'Usage: $0 -i path/to/epub.epub [options]' )
	.help( 'help' )
	.argv;

const epubPath = argv.i;
const shouldRemoveCover = !argv['keep-cover'];

// simple check on the name
if ( !epubPath.endsWith( '.epub' ) ) {
	console.error( `[ERROR] ${epubPath} isn't an EPUB file` );
	process.exit( 1 );
}

// variables used when parsing
const extractedFolderName = epubPath.substring( 0, epubPath.lastIndexOf( '.' ) );
let htmlFiles = [];
let opfFile = null; // raw opf
let opfFileAr = []; // parsed opf by line
let tocFile = null; // raw toc
let tocFileAr = []; // parsed toc by line
let navFile = null; // raw nav
let navFileAr = []; // parsed nav by line
let coverHTMLLocalPath = null;

// reading archives
const readZip = new AdmZip( epubPath );
const readZipEntries = readZip.getEntries(); // an array of ZipEntry records

// filter out any interesting files:
// - opf: main file
// - jpg/png/gif: cover files
// - html/xhtml: chapters etc (read for titles)
// - ncx: table of contents
const interestingFiles = readZipEntries.filter( entry => {
	const n = entry.entryName;
	return n.endsWith( 'html' ) ||
		n.endsWith( 'opf' ) ||
		n.endsWith( 'jpg' ) ||
		n.endsWith( 'png' ) ||
		n.endsWith( 'gif' ) ||
		n.endsWith( 'ncx' );
} ).map( entry => entry.entryName );

// find the opf
const opfFilePath = interestingFiles.find( file => file.endsWith( 'opf' ) );
if ( !opfFilePath ) {
	console.error( `[ERROR] No OPF file found: ${interestingFiles}` );
	process.exit( 2 );
}

// find the toc
const tocFilePath = interestingFiles.find( file => file.endsWith( 'ncx' ) );

// find the nav
const navFilePath = interestingFiles.find( file => file.endsWith( 'nav.xhtml' ) );

// extract all the files
const overwrite = true;
readZip.extractAllTo( extractedFolderName, overwrite );

Promise.all( interestingFiles
	// get all our html (content) files
	.filter( file => file.endsWith( 'xhtml' ) )
	// extract the titles, so we can look for relevant files later
	.map( file => fileReader.getH1Tags( `${extractedFolderName}/${file}` ) )
)
	// store the results
	.then( results => htmlFiles = results )
	// read our opf file
	.then( () => fileReader.readFile( `${extractedFolderName}/${opfFilePath}` ) )
	// store the opf file
	.then( opf => opfFile = opf )
	// parse it so we can easily find what we need
	.then( () => simpleXML.parse( opfFile.contents ) )
	// store the parsed file
	.then( opfAr => opfFileAr = opfAr )
	// remove the cover if needed
	.then( () => removeCoverFromOPF() )
	// add our guide data
	.then( () => addGuideDataToOPF() )
	// save the opf
	.then( () => {
		console.log( `[INFO] Saving OPF file` );
		const newOPF = simpleXML.rebuild( opfFileAr );
		return fs.promises.writeFile( opfFile.path, newOPF, 'utf-8' );
	} )
	// read our toc file (only if we're removing the cover)
	.then( () => {
		if ( shouldRemoveCover && tocFilePath ) {
			return updateTOC();
		}
	} )
	// read our nav file (only if we're removing the cover)
	.then( () => {
		if ( shouldRemoveCover && navFilePath && coverHTMLLocalPath ) {
			return updateNav();
		}
	} )
	// rezip our patched epub into a new version
	.then( () => {
		return new Promise( ( resolve, reject ) => {
			console.log( `[INFO] Rezipping EPUB` )

			const writeZip = new AdmZip();
			writeZip.addLocalFolder( extractedFolderName );
			writeZip.writeZip( `${extractedFolderName}_patched.epub`, e => {
				if ( e ) {
					console.error( `[ERROR] Couldn't rezip EPUB`, e );
				}
				resolve();
			} );
		} )
	} )
	// delete our unzipped folder
	.then( () => fs.promises.rmdir( extractedFolderName, { recursive: true } ) )
	.then( () => console.log( `[INFO] EPUB successfully patched` ) )
	.catch( e => {
		console.error( `[ERROR] Problem reading files`, e );
		process.exit( 3 );
	} )

// removes all cover entries from our opf file - returns a promise
function removeCoverFromOPF() {
	if ( !shouldRemoveCover ) {
		return Promise.resolve();
	}

	return new Promise( resolve => {
		console.log( `[INFO] Removing the cover and associated metadata` )

		// remove the image from the metadata
		let line = simpleXML.removeLineByAttr( opfFileAr, 'name', 'cover' );

		// remove the image from the manifest
		if ( line ) {
			const attribute = line.attributes.find( attr => attr.name === 'content' );
			line = simpleXML.removeLineByAttr( opfFileAr, 'id', attribute.value );

			// remove the actual image
			if ( line ) {
				const imageAttribute = line.attributes.find( attr => attr.name === 'href' );
				const imagePath = path.resolve( opfFile.folder, imageAttribute.value );
				fs.unlinkSync( imagePath );
			}
		}

		// remove from the guide
		simpleXML.removeLineByAttr( opfFileAr, 'type', 'cover' );

		// remove the page from the manifest
		line = simpleXML.removeLineByAttr( opfFileAr, 'id', 'cover_xhtml' );
		if ( line ) {
			const attribute = line.attributes.find( attr => attr.name === 'href' );
			coverHTMLLocalPath = attribute.value;
			const htmlPath = path.resolve( opfFile.folder, coverHTMLLocalPath );
			fs.unlinkSync( htmlPath );
		}

		// remove the data from the spine
		simpleXML.removeLineByAttr( opfFileAr, 'idref', 'cover_xhtml' );
		resolve();
	} )
}

// adds our guide data to our opf (dedication, about, etc)
function addGuideDataToOPF() {
	return new Promise( resolve => {
		const hasDedication = ( argv['ded-title'] );
		const hasAcknowledgements = ( argv['ack-title'] );
		const hasBook = ( argv['book-title'] );
		const hasBackOfBook = ( argv['bob-title'] );
		if ( !hasDedication && !hasAcknowledgements && !hasBook && !hasBackOfBook ) {
			resolve();
			return;
		}

		console.log( `[INFO] Adding guide data` );

		// get the index for the end of our guide
		const index = simpleXML.findIndexOfTag( opfFileAr, '/guide' );
		if ( index === -1 ) {
			console.warn( `[WARN] The OPF file doesn't contain a guide` );
			resolve();
			return;
		}

		// NOTE: when getting relative paths, we're using the posix submodule
		// to force foward slashes

		const extras = [];

		// dedication
		if ( hasDedication ) {
			extras.push( createReferenceEntry( 'ded-title', 'dedication' ) );
		}

		// ack
		if ( hasAcknowledgements ) {
			extras.push( createReferenceEntry( 'ack-title', 'acknowledgements' ) );
		}

		// book start
		if ( hasBook ) {
			extras.push( createReferenceEntry( 'book-title', 'text' ) );
		}

		// back of book
		if ( hasBackOfBook ) {
			extras.push( createReferenceEntry( 'bob-title', 'back-of-book' ) );
		}

		// add the extras to our file array (filter is used as we can have null entries)
		if ( extras.length > 0 ) {
			opfFileAr.splice( index, 0, ...extras.filter( item => item ) );
		}

		resolve();
	} )
}

// updates our toc file if we're removing the cover - returns a promise
function updateTOC() {
	return Promise.resolve()
		.then( () => fileReader.readFile( `${extractedFolderName}/${tocFilePath}` ) )
		// store the toc file
		.then( toc => tocFile = toc )
		// parse it so we can easily find what we need
		.then( () => simpleXML.parse( tocFile.contents ) )
		// store the parsed file
		.then( tocAr => tocFileAr = tocAr )
		// remove the cover
		.then( () => {
			simpleXML.removeLineByAttr( tocFileAr, 'name', 'cover' );
		} )
		// save the toc
		.then( () => {
			console.log( `[INFO] Saving TOC file` );
			const newTOC = simpleXML.rebuild( tocFileAr );
			return fs.promises.writeFile( tocFile.path, newTOC, 'utf-8' );
		} )
		.catch( e => {
			console.error( `[ERROR] Problem editing the TOC file`, e );
		} )
}

// updates our nav file if we're removing the cover - returns a promise
function updateNav() {
	return Promise.resolve()
		.then( () => fileReader.readFile( `${extractedFolderName}/${navFilePath}` ) )
		// store the nav file
		.then( nav => navFile = nav )
		// parse it so we can easily find what we need
		.then( () => simpleXML.parse( navFile.contents ) )
		// store the parsed file
		.then( navAr => navFileAr = navAr )
		// remove the cover
		.then( () => {
			// NOTE: as this may be wrapped in a <li> tag, we need to check first
			const index = simpleXML.findIndexByAttr( navFileAr, 'href', coverHTMLLocalPath );
			if ( index !== -1 ) {
				if ( navFileAr[index - 1].tag === 'li' ) {
					simpleXML.removeLinesAtIndex( navFileAr, index - 1, 3 ); // <li>, our cover tag, and </li>
				} else {
					simpleXML.removeLinesAtIndex( navFileAr, index, 1 );
				}
			}
		} )
		// save the nav
		.then( () => {
			console.log( `[INFO] Saving nav file` );
			const newNav = simpleXML.rebuild( navFileAr );
			return fs.promises.writeFile( navFile.path, newNav, 'utf-8' );
		} )
		.catch( e => {
			console.error( `[ERROR] Problem editing the nav file`, e );
		} )
}

// helper function for creating reference entries for our opf file
function createReferenceEntry( arg, type ) {
	// some whitespace, so that it lines up with the reset
	const whitespace = '    ';

	// NOTE: when getting relative paths, we're using the posix submodule
	// to force foward slashes

	if ( argv[arg] ) {
		// find the href for the file that has our title
		const filePath = fileReader.getPathForTitle( htmlFiles, argv[arg] );
		const filePathRel = ( filePath ) ? path.posix.relative( opfFile.folder, filePath ) : null;
		if ( filePathRel ) {
			return simpleXML.createLine( 'reference',
				{
					type,
					title: argv[`${arg}-title-opf`],
					href: filePathRel
				},
				whitespace
			)
		} else {
			console.warn( `[WARN] Couldn't find a file with the title '${argv[arg]} for the '${type}' reference` );
		}
	}
}