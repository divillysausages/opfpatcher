const reTagName = /<(\/?[^ >]+)/
const reAttributes = / ([^ =]+) ?= ?"([^"]+)"/g

// a *very* simple xml parser - breaks the file down to lines and pulls some
// data out of it so we can query it
exports.parse = function( contents ) {
	const lines = contents.split( '\n' );
	const parsed = [];
	lines.forEach( line => {
		const info = {
			raw: line,
		}
		parsed.push( info );

		// trim any spacing
		const trimmed = line.trim();

		// look for the tag first
		const tagAr = trimmed.match( reTagName );
		if ( !tagAr ) {
			return;
		}
		info.tag = tagAr[1];

		// pull out any attributes
		const attributesAr = trimmed.matchAll( reAttributes );
		info.attributes = [];
		for ( const att of attributesAr ) {
			info.attributes.push( {
				name: att[1],
				value: att[2]
			} )
		}
	} );

	return parsed;
}

// creates a line that we can add to our file
exports.createLine = function( tag, attrs, whitespace = '' ) {
	const attributes = [];

	let line = `${whitespace}<${tag}`;
	for ( let key in attrs ) {
		line += ` ${key}="${attrs[key]}"`
		attributes.push( {
			name: key,
			value: attrs[key]
		} )
	}
	line += `/>`;

	// create our object
	return {
		raw: line,
		tag,
		attributes
	}
}

// removes a line from the xml based on an attribute - returns the removed
// line object
exports.removeLineByAttr = function( parsedAr, attrName, attrValue ) {
	for ( let i = parsedAr.length - 1; i >= 0; i-- ) {
		const line = parsedAr[i];
		if ( !line.attributes ) {
			continue;
		}
		const found = line.attributes.find( attr => attr.name === attrName && attr.value === attrValue );
		if ( found ) {
			parsedAr.splice( i, 1 );
			return line;
		}
	}
	return null;
}

// removes a number of lines at a specific index - returns true or false
exports.removeLinesAtIndex = function( parsedAr, index, numToRemove ) {
	if ( index < 0 || index >= parsedAr.length ) {
		return false;
	}
	parsedAr.splice( index, numToRemove );
	return true;
}

// returns the index of a specific tag
exports.findIndexOfTag = function( parsedAr, tag ) {
	const len = parsedAr.length;
	for ( let i = 0; i < len; i++ ) {
		if ( parsedAr[i].tag === tag ) {
			return i;
		}
	}
	return -1;
}

// returns the index of a tag based on an attribute
exports.findIndexByAttr = function( parsedAr, attrName, attrValue ) {
	const len = parsedAr.length;
	for ( let i = 0; i < len; i++ ) {
		const line = parsedAr[i];
		if ( !line.attributes ) {
			continue;
		}
		const found = line.attributes.find( attr => attr.name === attrName && attr.value === attrValue );
		if ( found ) {
			return i;
		}
	}
	return -1;
}

// rebuilds our xml from our parsed array
exports.rebuild = function( parsedAr ) {
	return parsedAr
		.map( info => info.raw )
		.join( '\n' );
}