const fs = require( 'fs' );

const h1RE = /(<h1.{0,}>)([^\]]+)<\/h1>/g;
const idRE = /id ?= ?"([^"]+)/;

// reads a file in full
exports.readFile = ( path ) => {
	return fs.promises.readFile( path, 'utf-8' )
		.then( contents => {
			return {
				path,
				folder: _getFoldername( path ),
				filename: _getFilename( path ),
				filetype: _getFiletype( path ),
				contents
			}
		} )
}

// reads a file and extracts info on all the H1 tags in the doc
exports.getH1Tags = ( path ) => {
	return this.readFile( path )
		.then( info => {
			const a = info.contents.matchAll( h1RE );
			info.h1Tags = [];
			if ( a ) {
				for ( const match of a ) {
					info.h1Tags.push( {
						id: _getIDFromTag( match[1] ),
						title: match[2]
					} )
				}
			}
			return info;
		} )
}

// returns an item based on the title
exports.getPathForTitle = function( infoAr, title ) {
	const len = infoAr.length;
	for ( let i = 0; i < len; i++ ) {
		const info = infoAr[i];
		const numTags = info.h1Tags.length;
		for ( let j = 0; j < numTags; j++ ) {
			const tag = info.h1Tags[j];
			if ( tag.title.toLowerCase() === title.toLowerCase() ) {
				// if we have an id, add it to our path - this helps
				// in the case of our entire book being in one file
				if ( tag.id ) {
					return `${info.path}#${tag.id}`;
				}
				return info.path;
			}
		}
	}
	return null;
}

// returns the containing folder for a path
function _getFoldername( path ) {
	const index = path.lastIndexOf( '/' );
	return ( index !== -1 ) ? path.substring( 0, index ) : path;
}

// returns the file name of a file given its entire path
function _getFilename( path ) {
	const index = path.lastIndexOf( '/' );
	return ( index !== -1 ) ? path.substring( index + 1 ) : null;
}

// returns the file type of a file given its entire path
function _getFiletype( path ) {
	const index = path.lastIndexOf( '.' );
	return ( index !== -1 ) ? path.substring( index + 1 ) : null;
}

// given a tag like '<h1 id="chapter-1">1</h1>, it'll return
// "chapter-1" if an id exists
function _getIDFromTag( tag ) {
	const a = tag.match( idRE );
	return ( a ) ? a[1] : null;
}